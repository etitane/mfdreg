-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 07, 2022 at 02:07 AM
-- Server version: 5.7.26
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mfdreg`
--

-- --------------------------------------------------------

--
-- Table structure for table `pacienti`
--

DROP TABLE IF EXISTS `pacienti`;
CREATE TABLE IF NOT EXISTS `pacienti` (
  `p_id` int(10) NOT NULL AUTO_INCREMENT,
  `p_pcode` varchar(16) NOT NULL,
  `p_vards` varchar(32) NOT NULL,
  `p_uzvards` varchar(32) NOT NULL,
  `p_status` varchar(8) NOT NULL,
  `p_added` datetime NOT NULL,
  `p_changed` datetime NOT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pf_data`
--

DROP TABLE IF EXISTS `pf_data`;
CREATE TABLE IF NOT EXISTS `pf_data` (
  `pf_id` int(10) NOT NULL AUTO_INCREMENT,
  `pf_1` varchar(110) DEFAULT NULL,
  `pf_2` varchar(110) DEFAULT NULL,
  `pf_3` varchar(110) DEFAULT NULL,
  `pf_4` varchar(110) DEFAULT NULL,
  `pf_5` varchar(110) DEFAULT NULL,
  `pf_6` varchar(110) DEFAULT NULL,
  `pf_7` varchar(110) DEFAULT NULL,
  `pf_8` varchar(110) DEFAULT NULL,
  `pf_9` varchar(110) DEFAULT NULL,
  `pf_sin` varchar(110) DEFAULT NULL,
  `pf_dxt` varchar(110) DEFAULT NULL,
  `pf_brivas` varchar(110) DEFAULT NULL,
  `pf_sasaurinatas` varchar(110) DEFAULT NULL,
  `pf_saaugumi` varchar(110) DEFAULT NULL,
  `pf_veidojumi` varchar(110) DEFAULT NULL,
  `pf_nav` varchar(110) DEFAULT NULL,
  `pf_ir` varchar(110) DEFAULT NULL,
  `pf_irAtdalijumi` varchar(110) DEFAULT NULL,
  `pf_sarta` varchar(110) DEFAULT NULL,
  `pf_hipermeta` varchar(110) DEFAULT NULL,
  `pf_cianotiska` varchar(110) DEFAULT NULL,
  `pf_bala` varchar(110) DEFAULT NULL,
  `pf_tuskaina` varchar(110) DEFAULT NULL,
  `pf_atrofiska` varchar(110) DEFAULT NULL,
  `pf_hipertrofiska` varchar(110) DEFAULT NULL,
  `pf_perskods` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`pf_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
