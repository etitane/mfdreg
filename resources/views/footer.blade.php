        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>

        <script>

            $( function() {
                $( "#p_added" ).datepicker();
            } );

            var pacientCodeTimeout;
            function pacientSearch()
            {
                clearTimeout(pacientCodeTimeout);
                if($('#pacientCode').val().length >= 3)
                {                  
                    pacientCodeTimeout = setTimeout(function(){ 
                        $.get( "/pacientsearch/"+$('#pacientCode').val()).done(function( data ) {
                            eval( data );
                        });
                    }, 2000);
                }
            }

            function softDelete(id)
            {
                $.get( "/pacientsoftdelete/"+id).done(function( data ) {
                    eval( data );
                });
            }

            function hardDelete(id)
            {
                $.get( "/pacientharddelete/"+id).done(function( data ) {
                    eval( data );
                });
            }

            function toggleForm() {
                document.getElementById("add_patient").classList.toggle("show");
            }
            function toggleForm2() {
                document.getElementById("add_patient").classList.remove("show");
                document.getElementById("add_patient2").classList.add("show");
            }
            function closeForm2() {
                document.getElementById("add_patient2").classList.remove("show");
            }
            function disableSelect() {
                document.getElementById('irAtdalijumi').classList.add('hidden');
            }
            function enableSelect() {
                document.getElementById('irAtdalijumi').classList.remove('hidden');
            }
            function SendForm() {
                $.post( "/savepatient", {_token: "{{ csrf_token() }}", 
                p_added: $('#p_added').val(),
                p_pcode: $('#p_pcode').val(),
                p_vards: $('#p_vards').val(),
                p_uzvards: $('#p_uzvards').val(),
                pf_1: $('#pf_1').val(),
                pf_2: $('#pf_2').val(),
                pf_3: $('#pf_3').val(),
                sin: $('#sin').val(),
                dxt: $('#dxt').val(),
                pf_4: $('#pf_4').val(),
                pf_5: $('#pf_5').val(),
                brivas: $('#brivas').val(),
                sasaurinatas: $('#sasaurinatas').val(),
                saaugumi: $('#saaugumi').val(),
                veidojumi: $('#veidojumi').val(),
                nav: $('#nav').val(),
                ir: $('#ir').val(),
                irAtdalijumi: $('#irAtdalijumi').val(),
                pf_6: $('#pf_6').val(),
                pf_7: $('#pf_7').val(),
                sarta: $('#sarta').val(),
                hipermeta: $('#hipermeta').val(),
                cianotiska: $('#cianotiska').val(),
                bala: $('#bala').val(),
                tuskaina: $('#tuskaina').val(),
                atrofiska: $('#atrofiska').val(),
                hipertrofiska: $('#hipertrofiska').val(),
                pf_8: $('#pf_8').val(),
                pf_9: $('#pf_9').val(),
                 } ).done(function( data ) {
                    eval( data );
                });
                document.getElementById("add_patient2").classList.remove("show");
            }
        </script>
    </body>
</html>
