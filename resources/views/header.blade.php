<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>MFD : Pacientu reģistrs</title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
        <style>
    body {    
        margin: 0px;
        font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
    }

    .title {
        background-color: #000;
        color: #fff;
        padding: 20px 5%;
        line-height: 30px;
        font-size: 26px;
        font-weight: bold;
    }

    .content-box {
        margin: 20px 5%;
    }

    .basic-input {
        width: -webkit-fill-available;
        background-color: transparent;
        color: #000;
        border: 0px;
        border-bottom: 1px solid #000;
        padding: 6px 10px;
        font-size: 18px;
    }

    textarea {
        width: -webkit-fill-available;
        background-color: transparent;
        color: #000;
        border: 1px solid #000;
        padding: 6px 10px;
        font-size: 18px;
        border-radius: 2px;
        resize: none;
    }

    select {
        border: 0px;
        border-bottom: 1px solid #000;
    }

    td {
        border: 1px solid #000;
        margin: 0px;
        padding: 2px 5px;
    }

    .btn-delete {
        border: 0px;
        height: 18px;
        margin: 2px 0px;
        cursor: pointer;
    }

    .btn-primary {
        height: 100%; 
        border: 0px; 
        color: #fff; 
        background-color: #000; 
        font-size: 16px; 
        padding: 5px 15px; 
        cursor: pointer;
    }
    .popup {
        display: none;
        border: 1px solid #000;
        background-color: rgba(0, 0, 0, 0.4);
        position: fixed;
        top: 0px;
        z-index: 1;
        width: 100vw;
        height: 100vh;
    }

    .form {
        background-color: #fff;
        border-radius: 2px;
        height: fit-content;
        width: 60%;
        margin: auto;
        position: relative;
        top: 50%;
        transform: translateY(-50%);
        padding: 20px;
    }
    .show {
        display: block;
    }
    .hidden {
        display: none;
    }
</style>
    </head>

    <body>
        <div class="title">
            Pacientu reģistrs
        </div>