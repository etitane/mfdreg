@include('header')

@section('content')
<div class="content-box">
            <div style="display: flex; width: fit-content; height: 34px; width: 80%;">
                <div style="flex: 1;">
                    <input onKeyUp="pacientSearch();" id="pacientCode" class="basic-input" placeholder="Meklēšana pēc personālā koda">
                </div>
                <div style="flex: 1;">
                    <button class="btn-primary" OnClick="pacientSearch();">
                        Meklēt
                    </button>
                    <button onClick="toggleForm();" class="btn-primary" style="margin-left: 20px;">
                        Pievienot pacientu
                    </button>
                </div>
            </div>
            <div id="pacientData">
            </div>
</div>


<div class="popup" id="add_patient">
            <div class="form">
                <div style="margin-bottom: 15px; font-weight: bold; font-size: 18px;">
                    Informācija par pacientu
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="flex: 0.2; align-self: flex-end;">Datums:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <input id="p_added" class="basic-input">
                    </div>
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="flex: 0.2; align-self: flex-end;">Personas kods:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <input id="p_pcode" class="basic-input">
                    </div>
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="flex: 0.2; align-self: flex-end;">Vārds:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <input id="p_vards" class="basic-input">
                    </div>
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="flex: 0.2; align-self: flex-end;">Uzvārds:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <input id="p_uzvards" class="basic-input">
                    </div>
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="flex: 0.2; align-self: center; line-height: 16px;">Sūdzības un anamnēze:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <textarea id="pf_1"></textarea>
                    </div>
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="flex: 0.2; align-self: center; line-height: 16px;">Blakus slimības:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <textarea id="pf_2"></textarea>
                    </div>
                </div>
                <div style="display: flex; margin-bottom: 25px;">
                    <div style="flex: 0.2; align-self: center; line-height: 16px;">Medikamentu nepanesamība, alerģijas:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <textarea id="pf_3"></textarea>
                    </div>
                </div>
                <div style="margin-bottom: 15px; font-weight: bold; font-size: 18px;">
                    Deguns
                </div>
                <div style="display: flex; margin-bottom: 25px;">
                    <div style="flex: 0.2; align-self: center; line-height: 16px;">Ārējā forma</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <input type="radio" id="sin" name="areja_f">
                        <label for="sin">taisna</label>
                        <input type="radio" id="dxt" name="areja_f">
                        <label for="dxt">deformēta</label>
                    </div>
                </div>
                <button onClick="toggleForm();" class="btn-primary">
                    Atcelt
                </button>
                <button onClick="toggleForm2();" class="btn-primary" style="float: right; background-color: #00ab66;">
                    Turpināt
                </button>
            </div>
        </div>

        <div class="popup" id="add_patient2">
            <div class="form">
                <div style="margin-bottom: 15px; font-weight: bold; font-size: 18px;">
                    Deguns
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="flex: 0.2; align-self: flex-start;">Gļotāda:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <select id="pf_4">
                            <option>Sārta</option>
                            <option>Hiperemēta</option>
                            <option>Cianotiska</option>
                            <option>Bāla</option>
                        </select>
                        <select id="pf_5">
                            <option>Tūskaina</option>
                            <option>Atrofiska</option>
                            <option>Hipertrofiska</option>
                        </select>
                    </div>
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="flex: 0.2; align-self: flex-start;">Deguna ejas:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <input type="checkbox" id="brivas" name="deguna_ejas">
                        <label for="brivas">Brīvas</label>
                        <input type="checkbox" id="sasaurinatas" name="deguna_ejas">
                        <label for="sasaurinatas">Sašaurinātas</label>
                        <input type="checkbox" id="saaugumi" name="deguna_ejas">
                        <label for="saaugumi">Saaugumi</label>
                        <input type="checkbox" id="veidojumi" name="deguna_ejas">
                        <label for="veidojumi">Veidojumi</label>
                    </div>
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="flex: 0.2; align-self: flex-start;">Atdalījumi:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <input onClick="disableSelect();" type="radio" id="nav" name="atdalijumi">
                        <label for="nav">Nav</label>
                        <input onClick="enableSelect();" type="radio" id="ir" name="atdalijumi">
                        <label for="ir">Ir</label>
                        <select id="irAtdalijumi" class="hidden" style="margin-left: 10px;">
                            <option>Serozi</option>
                            <option>Gļotaini</option>
                            <option>Strutaini</option>
                            <option>Asiņaini</option>
                        </select>
                    </div>
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="flex: 0.2; align-self: flex-start; line-height: 16px;">Deguna starpsiena:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <select id="pf_6">
                            <option>Taisna</option>
                            <option>Deviēta</option>
                            <option>Perforēta</option>
                        </select>
                    </div>
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="flex: 0.2; align-self: flex-start; line-height: 16px;">Elpošana caur degunu:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <select id="pf_7">
                            <option>Brīva</option>
                            <option>Apgrūtināta</option>
                        </select>
                    </div>
                </div>
                <div style="margin-bottom: 15px; font-weight: bold; font-size: 18px;">
                    Mutes dobums
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="flex: 0.2; align-self: flex-start; line-height: 16px;">Rīkle:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <div style="margin-bottom: 5px;">
                            <input type="checkbox" id="sarta" name="rikle1">
                            <label for="sarta">Sārta</label>
                            <input type="checkbox" id="hipermeta" name="rikle1">
                            <label for="hipermeta">Hiperemēta</label>
                            <input type="checkbox" id="cianotiska" name="rikle1">
                            <label for="cianotiska">Cianotiska</label>
                            <input type="checkbox" id="bala" name="rikle1">
                            <label for="bala">Bāla</label>
                        </div>
                        <div>
                            <input type="checkbox" id="tuskaina" name="rikle2">
                            <label for="tuskaina">Tūskaina</label>
                            <input type="checkbox" id="atrofiska" name="rikle2">
                            <label for="atrofiska">Atrofiska</label>
                            <input type="checkbox" id="hipertrofiska" name="rikle2">
                            <label for="hipertrofiska">Hipertrofiska</label>
                        </div>
                    </div>
                </div>
                <div style="display: flex; margin-bottom: 15px;">
                    <div style="flex: 0.2; align-self: flex-start; line-height: 16px;">Mandeles:</div>
                    <div style="flex: 0.8; align-self: flex-end; padding-left: 5px;">
                        <select id="pf_8">
                            <option>Parasta lieluma</option>
                            <option>Palielinātas</option>
                        </select>
                        <select id="pf_9">
                            <option>Aplikums ir</option>
                            <option>Aplikuma nav</option>
                        </select>
                    </div>
                </div>
                <button onClick="closeForm2(); toggleForm();" class="btn-primary">
                    Atgriezties
                </button>
                <button onClick="closeForm2();" class="btn-primary">
                    Atcelt
                </button>
                <button onClick="SendForm();" class="btn-primary" style="float: right; background-color: #00ab66;">
                    Pievienot
                </button>
            </div>
        </div>

@show

@include('footer')