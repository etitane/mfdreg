<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('/pacientsearch/{personcode}', ['as' => 'pacient.search',  function ($personcode) {    
    $userSearched = DB::table('pacienti')->where('p_pcode', 'like','%'.htmlspecialchars($personcode,ENT_QUOTES).'%')->where('p_status','<>','DELETE')->orderByDesc('p_added')->limit(10)->get();    
    print "$('#pacientData').empty();";
    if(count($userSearched)>0)
    {
        $patientOut = "<table cellspacing=\"0\" style=\"width: 100%; margin-top: 50px;\"><tr style=\"background-color: #000; color: #fff;\"><th>Pers. kods</th><th>Vārds</th><th>Uzvārds</th><th>Piev. datums</th><th>Pārvietot arhivā</th><th>Dzēst</th></tr>";
        foreach ($userSearched as $patient) 
        {
            $patientOut .= "<tr id=\"pd".$patient->p_id."\"><td>".$patient->p_pcode."</td><td>".$patient->p_vards."</td><td>".$patient->p_uzvards."</td> <td>".$patient->p_added."</td><td style=\"text-align: center;\"><button class=\"btn-delete\" style=\"background-color: #ffbd44;\" OnClick=\"softDelete(".$patient->p_id.")\">x</button></td><td style=\"text-align: center;\"><button class=\"btn-delete\" style=\"background-color: #ff605c;\"  OnClick=\"hardDelete(".$patient->p_id.")\">x</button></td></tr>";
        }
        $patientOut .= "</table>";
        print "$('#pacientData').append('".$patientOut."');";
    }
    else
    {
        print "$('#pacientData').html('nav datu')";
    }
    die();
  }]);

  Route::get('/pacientsoftdelete/{person_id}', ['as' => 'pacient.softdelete',  function ($person_id) {    
    $userSoftDelete = DB::table('pacienti')->where('p_id', htmlspecialchars($person_id,ENT_QUOTES))->limit(1)->update(['p_status' => 'DELETE']);    
    print "$('#pd".$person_id."').empty();";
    die();
  }]);

  Route::get('/pacientharddelete/{person_id}', ['as' => 'pacient.softdelete',  function ($person_id) {    
    $userSoftDelete = DB::table('pacienti')->where('p_id', htmlspecialchars($person_id,ENT_QUOTES))->limit(1)->delete();    
    print "$('#pd".$person_id."').empty();";
    die();
  }]);


  Route::post('/savepatient', ['as' => 'pacient.save',  function () {
      $data = Request::post();
      try { 
        DB::table('pacienti')->insert([
            'p_pcode' => $data['p_pcode'],
            'p_vards' => $data['p_vards'],
            'p_uzvards' => $data['p_uzvards'],
            'p_status' => "OK",
            'p_added' => date('Y-m-d').' 00:00:00',
            'p_changed' => date('Y-m-d').' 00:00:00'       
        ]);
        DB::table('pf_data')->insert([
            'pf_1' => $data['pf_1'] ?? "",
            'pf_2' => $data['pf_2'] ?? "",
            'pf_3' => $data['pf_3'] ?? "",
            'pf_4' => $data['pf_4'] ?? "",
            'pf_5' => $data['pf_5'] ?? "",
            'pf_6' => $data['pf_6'] ?? "",
            'pf_7' => $data['pf_7'] ?? "",
            'pf_8' => $data['pf_8'] ?? "",
            'pf_9' => $data['pf_9'] ?? "",
            'pf_sin' => $data['pf_sin'] ?? "",
            'pf_dxt' => $data['pf_dxt'] ?? "",
            'pf_brivas' => $data['pf_brivas'] ?? "",
            'pf_sasaurinatas' => $data['pf_sasaurinatas'] ?? "",
            'pf_saaugumi' => $data['pf_saaugumi'] ?? "",
            'pf_veidojumi' => $data['pf_veidojumi'] ?? "",
            'pf_nav' => $data['pf_nav'] ?? "",
            'pf_ir' => $data['pf_ir'] ?? "",
            'pf_irAtdalijumi' => $data['pf_irAtdalijumi'] ?? "",
            'pf_sarta' => $data['pf_sarta'] ?? "",
            'pf_hipermeta' => $data['pf_hipermeta'] ?? "",
            'pf_cianotiska' => $data['pf_cianotiska'] ?? "",
            'pf_bala' => $data['pf_bala'] ?? "",
            'pf_tuskaina' => $data['pf_tuskaina'] ?? "",
            'pf_atrofiska' => $data['pf_atrofiska'] ?? "",
            'pf_hipertrofiska' => $data['pf_hipertrofiska'] ?? "",
            'pf_perskods' => $data['p_pcode'] ?? ""
        ]);
    } catch(\Illuminate\Database\QueryException $ex){ 
        die("alert('Kļuda');");
    }
    die();
  }]);
   